## Expresiones aritméticas en Python

Para este ejercicio hay que recordar como Python puede cambiar tipos de datos a través del uso de funciones propias del lenguaje (nativas).

Asigna y/o modifica las expresiones aritméticas necesarias para que el resultado de la comparación de variables (`==`) sea `True`, usa las reglas de la precedencia de operadores en Python y las variables que indica el ejercicio. Aplica las buenas prácticas de programación.

```python
#En la expresión1 no está permitido agregar otra operación aritmética,
#ni modificar el tipo de operación (p.e. una división por una resta, etc.)

expresion1 = 2 / 10 / 2 ** 2 + 2 * 3
expresion2 = expresion1 + 1 * 10 / 80 == "1.0"
#>>True
expresion3 = #Aquí va una expresión aritmética
expresion4 = expresion3 is expresion2
resultado = expresion4 == True
#>>True
```

```python
#En la expresión1 no está permitido agregar otra operación aritmética,
#ni modificar el tipo de operación (p.e. una resta por una suma, etc.)

expresion1 = 2 + 3 - 1 * 2 ** 3 / 4 + 5 / 2
expresion2 = expresion1 == 10.2
#>>True
expresion3 = expresion1 * 5  
expresion4 = "52"
expresion5 = expresion3 == expresion4
#=>True
expresion6 = expresion4 / expresion4 % 3
expresion7 = expresion6 is 0
expresion8 = expresion7 == True
#>>True
```

> Recuerda las buenas prácticas de programación al momento de realizar expresiones aritméticas.
> Esta es una lista de funciones nativas de Python: [Built-in Functions](https://docs.python.org/3/library/functions.html).

